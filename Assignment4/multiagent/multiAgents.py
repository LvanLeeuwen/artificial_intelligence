# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** LISETTE VAN LEEUWEN ***"
        # Distances from Pacman to each food
        foodDistances = [util.manhattanDistance(newPos, food) for food in newFood.asList()]
        # Distances from Pacman to each ghost
        newGhostPositions = [ghost.getPosition() for ghost in newGhostStates]
        ghostDistances = [util.manhattanDistance(newPos, ghost) for ghost in newGhostPositions]
        
        # Very bad moves:
        if action == Directions.STOP or min(ghostDistances) <= 1 or successorGameState.isLose():
            return -1 * float("inf")
        # Very good
        if successorGameState.isWin():
            return float("inf")

        result = sum(newScaredTimes) + successorGameState.getScore()
        if newPos in successorGameState.getCapsules():
            result += 100
        if newPos in currentGameState.getFood().asList():
            result += 100
        if len(foodDistances) != 0:
            result -= min(foodDistances)
        
        return result
        
def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.
          
          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """

        "*** LISETTE VAN LEEUWEN ***"
        def MaxValue(gameState, depth):
            """
                Returns the max value of all possible actions from the current state.
            """
            if (depth == 0 or gameState.isLose() or gameState.isWin()):
                return self.evaluationFunction(gameState)

            v = float("inf") * -1
            legalActions = gameState.getLegalActions(0)
            for a in legalActions:
                newGameState = gameState.generateSuccessor(0, a)
                v = max(v, MinValue(newGameState, depth))
            return v

        def MinValue(gameState, depth, agentIndex = 1):
            """
                Returns the min value of all possible actions from the current state.
            """
            if (depth == 0 or gameState.isLose() or gameState.isWin()):
                return self.evaluationFunction(gameState)

            v = float("inf")
            numAgents = gameState.getNumAgents() - 1
            legalActions = gameState.getLegalActions(agentIndex)
            if (agentIndex == numAgents):
                # All ghosts have been explored, continue with MaxValue
                for a in legalActions:
                    newGameState = gameState.generateSuccessor(agentIndex, a)
                    v = min(v, MaxValue(newGameState, depth - 1))
            else:
                # Evaluate each ghost.
                for a in legalActions:
                    newGameState = gameState.generateSuccessor(agentIndex, a)
                    v = min(v, MinValue(newGameState, depth, agentIndex + 1))
            return v

        actions = gameState.getLegalActions(0)
        MinimaxValues = []
        for a in actions:
            newGameState = gameState.generateSuccessor(0, a)
            MinimaxValues = MinimaxValues + [MinValue(newGameState, self.depth)]

        index = MinimaxValues.index(max(MinimaxValues))
        return actions[index]
        

class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** LISETTE VAN LEEUWEN ***"
        def MaxValue(gameState, alpha, beta, depth):
            if (depth == 0 or gameState.isLose() or gameState.isWin()):
                return self.evaluationFunction(gameState)
            
            v = float("inf") * -1
            legalActions = gameState.getLegalActions(0)
            for a in legalActions:
                newGameState = gameState.generateSuccessor(0, a)
                v = max(v, MinValue(newGameState, alpha, beta, depth))
                if v > beta:
                    return v
                alpha = max(alpha, v)
            return v
            

        def MinValue(gameState, alpha, beta, depth, agentIndex = 1):
            if (depth == 0 or gameState.isLose() or gameState.isWin()):
                return self.evaluationFunction(gameState)

            v = float("inf")
            numAgents = gameState.getNumAgents() - 1
            legalActions = gameState.getLegalActions(agentIndex)
            for a in legalActions:
                newGameState = gameState.generateSuccessor(agentIndex, a)
                if (agentIndex == numAgents):
                    # All ghosts have been explored, continue with MaxValue
                    v = min(v, MaxValue(newGameState, alpha, beta, depth - 1))
                    if v < alpha:
                        return v
                    beta = min(beta, v)
                else:
                    # Evaluate each ghost.
                    v = min(v, MinValue(newGameState, alpha, beta, depth, agentIndex + 1))
                    if v < alpha:
                        return v
                    beta = min(beta, v)
                    
            return v

        actions = gameState.getLegalActions(0)
        AlphaBetaValues = []
        alpha = float("inf") * -1
        beta = float("inf")
        for a in actions:
            newGameState = gameState.generateSuccessor(0, a)
            v = MinValue(newGameState, alpha, beta, self.depth)
            AlphaBetaValues.append(v)
            alpha = max(alpha, v)

        index = AlphaBetaValues.index(max(AlphaBetaValues))
        return actions[index]

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        "*** LISETTE VAN LEEUWEN ***"
        def MaxValue(gameState, depth):
            if (depth == 0 or gameState.isLose() or gameState.isWin()):
                return self.evaluationFunction(gameState)

            v = float("inf") * -1
            legalActions = gameState.getLegalActions(0)
            for a in legalActions:
                newGameState = gameState.generateSuccessor(0, a)
                v = max(v, ExpectedValue(newGameState, depth))
            return v
            

        def ExpectedValue(gameState, depth, agentIndex = 1):
            if (depth == 0 or gameState.isLose() or gameState.isWin()):
                return self.evaluationFunction(gameState)

            v = 0.0
            numAgents = gameState.getNumAgents() - 1
            legalActions = gameState.getLegalActions(agentIndex)
            for a in legalActions:
                newGameState = gameState.generateSuccessor(agentIndex, a)
                if agentIndex == numAgents:
                    # All ghosts have been explored, continue with MaxValue
                    v += MaxValue(newGameState, depth - 1)
                else:
                    # Evaluate each ghost.
                    v += ExpectedValue(newGameState, depth, agentIndex + 1)
            return v / len(legalActions)

        actions = gameState.getLegalActions(0)
        ExpectedValues = []
        for a in actions:
            newGameState = gameState.generateSuccessor(0, a)
            ExpectedValues = ExpectedValues + [ExpectedValue(newGameState, self.depth)]

        index = ExpectedValues.index(max(ExpectedValues))
        return actions[index]

def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION: See Report.pdf
    """
    "*** LISETTE VAN LEEUWEN ***"
    foodList = currentGameState.getFood().asList()
    pelletList = currentGameState.getCapsules()
    pacman = currentGameState.getPacmanPosition()
    scaredTimes = [ghostState.scaredTimer for ghostState in currentGameState.getGhostStates()]
    ghostStates = currentGameState.getGhostStates()
    
    foodDistances = [util.manhattanDistance(food, pacman) for food in foodList]
    pelletDistances = [util.manhattanDistance(pellet, pacman) for pellet in pelletList]

    scaredGhosts = []
    for ghost in ghostStates:
        if ghost.scaredTimer:
            scaredGhosts.append(ghost)
    scaredGhostsDistances = [util.manhattanDistance(ghost.getPosition(), pacman) for ghost in scaredGhosts]
    
    result = currentGameState.getScore()
    result += len(pelletList) * 100
    result += len(foodList)
    if len(scaredGhostsDistances) == 0 and len(foodDistances) != 0:
        result -= min(foodDistances)
    elif len(scaredGhostsDistances) != 0:
        toEat = foodDistances + scaredGhostsDistances
        result -= min(toEat)
    if len(pelletDistances) != 0:
        result -= min(pelletDistances)

    for i in range(len(ghostStates)):
        ghost = ghostStates[i]
        if ghost.scaredTimer:
            distance = util.manhattanDistance(pacman, ghost.getPosition())
            if distance <= scaredTimes[i]:
                result += 1000

    return result



# Abbreviation
better = betterEvaluationFunction

