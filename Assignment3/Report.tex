\documentclass[a4paper]{article}

%----------------------------------------------------------------------------------------------
% PACKAGES
%----------------------------------------------------------------------------------------------

%\usepackage[a4paper, left=3cm, right=3cm, top=2.5cm, bottom=2.5cm]{geometry}
\usepackage{graphicx}
\usepackage[british]{babel}
\usepackage[hidelinks]{hyperref}
\usepackage[usenames, dvipsnames]{color}
\usepackage[document]{ragged2e}
\usepackage{amsmath}
\usepackage{tikz}
\usetikzlibrary{arrows}

%----------------------------------------------------------------------------------------------
% HYPHENATION RULES
%----------------------------------------------------------------------------------------------

%----------------------------------------------------------------------------------------------
% DOCUMENT INFORMATION
%----------------------------------------------------------------------------------------------

\title{ %
	Artificial Intelligence \\
	\large Assignment 3: Constraint Satisfaction Problems
	}
\author{ %
	Lisette van Leeuwen \\
	20141290
	}
%----------------------------------------------------------------------------------------------

\begin{document}
	\maketitle
	
	\section*{Problem 1}
		We have been asked to design a menu for the student restaurant. Four dishes will be prepared being a soup, a main course, a vegetarian dish and a grill dish. These will be our variables presented as $X = \{S, M, V, G\}$. For each dish we need to choose one of the following recipes:
		\begin{itemize}
			\item \textbf{S}: Chinese Tomato, Chicken, or Pumpkin
			\item \textbf{M}: Merguez, Codfish, or Pizza
			\item \textbf{V}: Quorn Stew, Tabouleh, or Spring Rolls
			\item \textbf{G}: Sausage, Steak, or Tuna
		\end{itemize}
		We translate these to the following domains:
		\begin{equation*}
			D_S = \{to, ch, pu\} \qquad
			D_M = \{me, co, pi\} \qquad
			D_V = \{qu, ta, sr\} \qquad
			D_G = \{sa, st, tu\} 
		\end{equation*}
		The chef asked us to consider the following constraints:
		\begin{itemize}
			\item $C_{(S, M)}$ = The menu should include at least one option which contains pumpkin. The dishes containing pumpkin are pumpkin soup, merguez and codfish.
			\item $C_{(S, V)}$ = If you choose the Chinese Tomato soup, you cannot combine it with the Tabouleh or Spring Rolls.
			\item $C_{(V, G)}$ = You should not combine the expensive Quorn Stew with the Steak or Tuna.
		\end{itemize}
		Using these constraints we can construct the following constraint graph: \\\bigskip
		\begin{tikzpicture}[-=stealth',shorten >=1pt,auto,node distance=3cm,
                   thick,main node/.style={circle,draw,font=\sffamily\Large\bfseries}]
		
		  \node[main node] (1) {S};
		  \node[main node] (2) [right of=1] {M};
		  \node[main node] (3) [below of=1] {V};
		  \node[main node] (4) [right of=3] {G};
				
		  \path[every node/.style={font=\sffamily\small}]
		   (1) 	edge node {} (2)
			    edge node {} (3)
		   (3) 	edge node {} (4);
		\end{tikzpicture} \\\medskip
		Imagine we first choose Chinese tomato soup. Using forward checking, we get the following domains: \\\medskip
		\begin{tabular}{|l|l|l|l|l|}
			\hline
			\textbf{Variable} & S & M & V & G \\
			\hline
			\textbf{Domain} & \{to, ch, pu\} & \{me, co, pi\} & \{qu, ta, sr\} & \{sa, st, tu\} \\
			\hline
			\textbf{S = to} & \{to\} & \{me, co\} & \{qu\} & \{sa, st, tu\} \\
			\hline
		\end{tabular} \\\medskip
		When we use arc consistency, we obtain different domains: \\\medskip
		\begin{tabular}{|l|l|l|l|l|}
			\hline
			\textbf{Variable} & S & M & V & G \\
			\hline
			\textbf{Domain} & \{to, ch, pu\} & \{me, co, pi\} & \{qu, ta, sr\} & \{sa, st, tu\} \\
			\hline
			\textbf{S = to} & \{to\} & \{me, co\} & \{qu\} & \{sa\} \\
			\hline
		\end{tabular} \\\medskip
		After using arc consistency we already see a solution forming. We only need to choose a recipe for the main course. The merguez and codfish both satisfy the first constraint so either one of them leads to a solution. There are of course more solutions. If we chose S = pu, then we can choose all values from the initial domain of M. The first constraint tells us we must have at least one dish with pumpkin, so we can also have multiple dishes with pumpkin. We find three unique solutions if we choose V = qu and G = sa. These solutions are: \\\medskip
		\begin{tabular}{l|l|l|l}
			S & M & V & G \\
			\hline
			to & me & qu & sa \\
			to & co & qu & sa \\
			pu & me & qu & sa \\
			pu & co & qu & sa \\
			pu & pi & qu & sa
		\end{tabular} \\\medskip
		If we chose V $\neq$ qu then we can choose every value from the initial domain of G. This leads to another nine unique solutions. The following table lists all possible solutions in a somewhat compact form. \\\medskip
		\begin{tabular}{|l|l|l|l|l|}
			\hline
			\textbf{S} & \textbf{M} & \textbf{V} & \textbf{G} & \textbf{Number of solutions}\\
			\hline
			to & \{me, co\} & qu & sa & 2\\
			\hline
			ch & \{me, co\} & qu & sa & 2\\
			\hline
			ch & \{me, co\} & ta & \{sa, st, tu\} & 6\\
			\hline
			ch & \{me, co\} & sr & \{sa, st, tu\} & 6\\
			\hline
			pu & \{me, co, pi\} & qu & sa & 3\\
			\hline
			pu & \{me, co, pi\} & ta & \{sa, st, tu\} & 9\\
			\hline
			pu & \{me, co, pi\} & sr & \{sa, st, tu\} & 9\\
			\hline
		\end{tabular} \\\bigskip
		An arc $X \rightarrow Y$ is consistent if for every value \textit{x} of \textit{X} there is some allowed value \textit{y} in \textit{Y}. If we choose a value for \textit{Y} we want to check if the arc $X \rightarrow Y$ is still consistent. This may not be the case, in which we need to prune the domain values of \textit{X}. Forward checking stops after this step. Arc consistency continues to check if the changes in \textit{X} affect any other variables. The domains of those variables may then also be pruned. \\
		Since forward checking is the first step of arc-consistency, arc-consistency will always prune the same amount of domain values as forward checking. Arc consistency continues its evaluation of the variables and may prune more domain values. We conclude that arc-consistency always prunes at least as many domain values as forward checking. 
			
	\newpage
	\section*{Problem 2}
		For problem 2 we are given a static situation of the game Minesweeper. In this game we have fifteen variables, being the empty squares. The domain of each variable is a boolean value. The value true represents a bomb and the value false means the square doesn't contain a bomb. The numbers on the grid form the constraints. Lets represent the variables with $V = \{v_1, ..., v_n\}$ and the constraints with $C = \{c_1, ..., c_m\}$. We have the following grid: \\\medskip
		\begin{tabular}{|c|c|c|c|}
			\hline
			$v_1$ & $v_2$ & \textcolor{ForestGreen}{$c_1$} & $v_3$ \\
			\hline
			\textcolor{ForestGreen}{$c_2$} & \textcolor{Red}{$c_3$} & $v_4$ & $v_5$ \\
			\hline
			$v_6$ & $v_7$ & $v_8$ & $v_9$ \\
			\hline
			$v_{10}$ & \textcolor{ForestGreen}{$c_4$} & $v_{11}$ & \textcolor{blue}{$c_5$} \\
			\hline
			$v_{12}$ & \textcolor{blue}{$c_6$} & $v_{13}$ & $v_{14}$ \\
			\hline
		\end{tabular} \\\medskip
		We get the following constraints:
		\begin{itemize}
			\item $c_1$: two of the variables $\{v_2, v_3, v_4, v_5\}$ contain a bomb.
			\item $c_2$: two of the variables $\{v_1, v_2, v_6, v_7\}$ contain a bomb.
			\item $c_3$: three of the variables $\{v_1, v_2, v_4, v_6, v_7, v_8\}$ contain a bomb.
			\item $c_4$: two of the variables $\{v_6, v_7, v_8, v_{10}, v_{11}, v_{12}, v_{13}\}$ contain a bomb.
			\item $c_5$: one of the variables $\{v_8, v_9, v_{11}, v_{13}, v_{14}\}$ contains a bomb.
			\item $c_6$: one of the variables $\{v_{10}, v_{11}, v_{12}, v_{13}\}$ contains a bomb.
		\end{itemize}
			
\end{document}