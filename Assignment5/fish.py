import os
import numpy as np
import cv2
import cPickle
import gzip
import argparse
from sklearn.neighbors import NearestNeighbors
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.cluster import KMeans
from sklearn.cluster import MiniBatchKMeans
from sklearn.metrics import log_loss
from random import shuffle
import datetime
import sys

# Fish Labels. NoF corresponds to No Fish, others are all types of Fish
LABELS = {
    "ALB" : 0,
    "BET" : 1,
    "DOL" : 2,
    "LAG" : 3,
    "NoF" : 4,
    "OTHER" : 5,
    "SHARK" : 6,
    "YFT" : 7,
}

class FishClassifier:
    
    def __init__(self, MULTI=False, WRITE=False, READ=False, TEST=False, ASSOCIATION=False):
        self.multi = MULTI
        self.write = WRITE
        self.association = ASSOCIATION

        path = os.path.dirname(os.path.abspath(__file__))
        self.sift = dict()

        # Read the stored SIFT features from file SIFT.p
        if READ:
            self.readSIFTFromFile()

        # Creates dictionary that maps every label to a list of files with that label
        self.dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'Train')
        self.labelFiles = dict()
        self.logloss = list()

        if not TEST:
            # Get all file labels
            for label in LABELS.keys():
                self.labelFiles[label] = [os.path.join(self.dir, label, ff) \
                                         for ff in filter(lambda fn: fn.endswith(".jpg"), \
                                            os.listdir(os.path.join(self.dir, label)))]
        else:
            # Get 10% of the file labels
            if READ:
                for label in LABELS.keys():
                    self.labelFiles[label] = list()
                    for filename in self.sift.keys():
                        if filename.find(label) != -1:
                            self.labelFiles[label].append(filename)
            else:
                testPercent = 10
                testMinimum = 10
                for label in LABELS.keys():
                    self.labelFiles[label] = list()
                    i = 0
                    number_of_files = len(os.listdir(os.path.join(self.dir, label)))
                    for ff in filter(lambda fn: fn.endswith(".jpg"), os.listdir(os.path.join(self.dir, label))):
                        if i < testMinimum:
                            self.labelFiles[label].append(os.path.join(self.dir, label, ff))
                            i += 1
                        elif i < round(number_of_files * testPercent / 100.0):
                            self.labelFiles[label].append(os.path.join(self.dir, label, ff))
                            i += 1
        if not READ or not ASSOCIATION:
            self.getSIFT()


    def crossValidation(self, k):
        """
            Divide the data in k sets using k-crossvalidation.
            Predict for each image de fish class.
            Check if the prediction is correct.
            Investigate the accuracies and the log loss.
        """
        scores = []
        
        files = dict()
        for i in range(0, k):
            files[i] = list()
        subset = 0
        
        # Fill the k sets using Round Robin
        for key, value in self.labelFiles.iteritems():
            shuffle(value)
            for item in value:
                if self.multi:
                    files[subset].append((item, LABELS[key]))
                else:
                    if key == "NoF":
                        files[subset].append((item, 0))
                    else:
                        files[subset].append((item, 1))
                    
                # Update next subset to use
                subset = (subset + 1) % k
        
        # Create the train and test data
        for i in range(0, k):
            train = []
            test = []
            testLabels = []
            
            # Exclude the ith set from the train data 
            for j in range(0, k):
                if j != i:
                    for x in files[j]:
                        train.append(x)
            # The ith set is the test data
            for j in files[i]:
                test.append(j)
                testLabels.append(j[1])
            
            # Do the prediction
            prediction = list()
            if not self.association:
                prediction = self.predict(train, test)
            else:
                prediction = self.association_rules(train, test)

            if self.multi:
                scores.append(self.scoreMulti(prediction, testLabels))
            else:
                scores.append(self.score(prediction, testLabels))
        print scores

        if self.multi:
            # Investigate accuracies per class
            result = len(LABELS) * [0]
            for s in scores:
                for i in range(0, len(s)):
                    result[i] += s[i]
            accuracies = [r / len(scores) for r in result]
        else:
            accuracies = np.mean([(x1+x2)/2.0 for (x1, x2) in scores])
        
        print "Mean Accuracy:", accuracies
        if self.multi:
            print "Mean Log Loss:", np.mean(self.logloss)

        
    def predict(self, train, test):
        """
            Predict for each image in test the fish classes. 
            Cluster on the SIFT features and create feature vectors.
            Add simple image features to the feature vectors.
            Use a classifier to predict the fish classes.
        """
        # Get the keypoints
        trainKeypoints = self.getKeypoints(train)
        
        # Compute the clusters
        k = 200
        kmeans = MiniBatchKMeans(n_clusters=k, init='random').fit(trainKeypoints)
        
        # Which keypoint belongs to which cluster?
        trainFeatures = self.getClusters(kmeans, train, k)
        testFeatures = self.getClusters(kmeans, test, k)
        
        # Add simple image features to feature vector
        trainFeatures = self.getFeatures(train, trainFeatures)
        testFeatures = self.getFeatures(test, testFeatures)
        
        # X contains all lists of number of features per cluster per image
        # y contains the fish class
        # testF contains the testdata
        X, y, testF = [], [], []
        for filename, isFish in train:
            X.append(trainFeatures[filename])
            y.append(isFish)
        for filename, isFish in test:
            testF.append(testFeatures[filename])

        # CLASSIFICATION
        #classifier = KNeighborsClassifier(n_neighbors=11, weights='distance') # Choose k odd
        #classifier = GaussianNB()
        classifier = RandomForestClassifier(n_estimators=100, criterion='entropy', max_features='log2')
        classifier.fit(X, y)

        if self.multi:
            probabilities = classifier.predict_proba(testF)
            self.getLogLoss(probabilities, test)
            return [np.argmax(p) for p in probabilities]
        else:
            return classifier.predict(testF)
        
        
    def score(self, prediction, testLabels):
        # Computes predictive accuracy for both classes;
        # Overall score is average of these 2 accuracies
        # This handles class imbalance on the scoring side
        zeroCount, zeroCorr, oneCount, oneCorr = 0, 0, 0, 0
        for i in range(len(testLabels)):
            if testLabels[i] == 0:
                if prediction[i] == 0:
                    zeroCorr += 1
                zeroCount += 1
            elif testLabels[i] == 1:
                if prediction[i] == 1:
                    oneCorr += 1
                oneCount += 1
        return (zeroCorr/float(zeroCount), oneCorr/float(oneCount))

    def scoreMulti(self, prediction, testLabels):
        """
            Computes predictive accuracy for multiple classes.
        """
        counts = dict()
        corrects = dict()
        for lab in LABELS.keys():
            c = LABELS[lab]
            counts[c] = 0
            corrects[c] = 0
        
        for i in range(len(testLabels)):
            l = testLabels[i]
            p = prediction[i]
            counts[l] += 1
            if l == p:
                corrects[l] += 1
        return tuple([corrects[c]/float(counts[c]) for c in counts.keys()])


    def getFeatures(self, data, featureVector):
        """
            Add simple image features to the feature vector.
        """
        for filename, isFish in data:
            img = cv2.imread(filename)
            img_r = cv2.resize(img,(1280,720))

            # Count of pixel intensities in grayscale image
            gray = cv2.cvtColor(img_r, cv2.COLOR_BGR2GRAY) 
            # Thresholds
            retval, thrs = cv2.threshold(gray, 1, 100, cv2.THRESH_TOZERO)
            # Blurred image
            blur = cv2.blur(thrs, (5, 5))
            # Edges
            edge = cv2.Canny(blur, 3, 5)
            
            features = [np.sum(np.array(gray)), np.sum(np.array(thrs)), np.sum(np.array(blur)), np.sum(np.array(edge))]
            np.append(featureVector[filename], features)
        return featureVector
        
    def getKeypoints(self, data):
        """
            Return a list of keypoints.
        """
        out = list()
        for filename, isFish in data:
            for kp in self.sift[filename]:
                out.append(kp)
        return out

    def getClusters(self, kmeans, data, k):
        """ 
            Create the feature vector.
            Each vector contains the number of keypoints per cluster per image.
        """
        out = dict()
        for filename, isFish in data:
            out[filename] = k * [0]
            for kp in self.sift[filename]:
                cluster = kmeans.predict(kp.reshape(1, -1))
                out[filename][cluster[0]] += 1
        return out

    def getLogLoss(self, probabilities, test):
        testLabels = list()
        for filename, isFish in test:
            testLabels.append(isFish)
        loss = log_loss(testLabels, probabilities)
        self.logloss.append(loss)

    def getSIFT(self):
        """
            Get the SIFT features and optionally write to a file.
        """
        for label in LABELS.keys():
            for image in self.labelFiles[label]:
                # Read in image, resize all images to same size (currently hardcoded)
                img = cv2.imread(image)
                img_r = cv2.resize(img,(1280,720))
                # Count of pixel intensities in grayscale image
                gray = cv2.cvtColor(img_r, cv2.COLOR_BGR2GRAY) 
                sift = cv2.SIFT()
                kp, des = sift.detectAndCompute(gray, None)
                self.sift[image] = des

        if self.write:
            pickfile = os.path.dirname(os.path.abspath(__file__)) + '/SIFT.p'
            with gzip.open(pickfile, 'wb') as f:
                cPickle.dump(self.sift, f)
        
    def readSIFTFromFile(self):
        """
            Read the SIFT features from the file SIFT.p
        """
        pickfile = os.path.dirname(os.path.abspath(__file__)) + '/SIFT.p'
        with gzip.open(pickfile, 'rb') as f:
            self.sift = cPickle.load(f)
        

    def getLabel(self, value):
        """
            Determine the label based on a value.
        """
        if self.multi:
            for key in LABELS.keys():
                if LABELS[key] == value:
                    return key
        else:
            if value == 0:
                return 'NoF'
            else:
                return 'F'

    def getHistograms(self, data):
        """
            Create histograms for each image in the dataset.
        """
        histograms = list()
        for i in range(0, len(data)):
            filename, isFish = data[i]
            label = self.getLabel(isFish)
            histograms.append([])

            # Now only on gray images
            img = cv2.imread(filename)
            img_r = cv2.resize(img, (1280, 720))
            gray = cv2.cvtColor(img_r, cv2.COLOR_BGR2GRAY)
            hist_item = cv2.calcHist([gray],[0],None,[256],[0,256])

            for item in hist_item:
                histograms[i].append(item[0])
            histograms[i].append(label)
        return histograms

    def association_rules(self, train, test):
        """
            Create histograms from the training data.
            Make a transaction database containing the itemsets and the support.
            Mine association rules from the transaction database.
            Create histograms from the test data
            Check if the histograms match an association rule
            Determine the probabilities and log loss
        """
        # Compute histograms from training data
        histograms = self.getHistograms(train)
        labels = dict()
        if self.multi:
            labels = LABELS
        else:
            labels = {'NoF' : 0, 'F' : 1}
        # Compute transaction database
        transactionDatabase = eclat(histograms, 50)
        # Mine association rules
        associationRules = list()
        for i in range(0, len(transactionDatabase)):
            itemset, support = transactionDatabase[i]
            for index, value in itemset:
                if value in labels.keys():
                    for j in range(0, len(transactionDatabase)):
                        if i == j:
                            continue
                        conf = float(support) / float(transactionDatabase[j][1])
                        rule = (transactionDatabase[j][0], itemset, conf, value)
                        if rule not in associationRules:
                            associationRules.append(rule)
        # Get histograms from test data
        testHistograms = self.getHistograms(test)
        # Use association rules on test histograms
        result = list()
        probabilities = list()
        for i in range(0, len(testHistograms)):
            hist = testHistograms[i]
            entry = [0] * len(labels.keys())
            # Check if a rule matches the histogram
            for lhs, rhs, conf, l in associationRules:
                labelindex = labels[l]
                if match(lhs, hist):
                    if conf > entry[labelindex]:
                        entry[labelindex] = conf
            probabilities.append(entry)
        # Normalise probabilities
        for prob in probabilities:
            if sum(prob) > 0:
                for i in range(len(prob)):
                    prob[i] = float(prob[i]) / sum(prob)
        self.getLogLoss(probabilities, test)
        return [np.argmax(p) for p in probabilities]

def match(pattern, tup):
    for (ix, val) in pattern:
        if tup[ix] != val: return False
    return True
    
def getAttrFromItemset(itemset, attrIx):
    for (ix, val) in itemset:
        if ix == attrIx: return val
    return -1
    
# Expects a matrix where every row is a transaction
# Returns a list of (itemset, support) pairs
# Itemset is a list of (attrIndex, value) pairs
def eclat(transactionTable, minsup):
    data = {}
    for rowIx in range(len(transactionTable)):
        row = transactionTable[rowIx]
        for item in enumerate(row):
            if not item in data:
                data[item] = set([rowIx])
            else:
                data[item].add(rowIx)
    return eclatImpl([], data.items(), minsup)
    
# http://adrem.ua.ac.be/~goethals/software/files/eclat.py
def eclatImpl(prefix, items, minsup):
    output = []
    while items:
        i,itids = items.pop()
        isupp = len(itids)
        if isupp >= minsup:
            output.append((sorted(prefix+[i]), isupp))
            suffix = [] 
            for j, ojtids in items:
                jtids = itids & ojtids
                if len(jtids) >= minsup:
                    suffix.append((j,jtids))
            output += eclatImpl(prefix+[i], sorted(suffix, key=lambda item: len(item[1]), reverse=True), minsup)
    return output

if __name__ == '__main__':
    # Create argument parser
    parser = argparse.ArgumentParser(description='Classify fish')
    # Add a flag for executing the test case
    parser.add_argument('-t', dest='test', action='store_true')
    parser.add_argument('-m', dest='multi', action='store_true')
    parser.add_argument('-w', dest='write', action='store_true')
    parser.add_argument('-r', dest='read', action='store_true')
    parser.add_argument('-a', dest='association', action='store_true')

    args = parser.parse_args()
    
    print datetime.datetime.now().time()
    fc = FishClassifier(MULTI=args.multi, WRITE=args.write, READ=args.read, TEST=args.test, ASSOCIATION=args.association)
    fc.crossValidation(10)
    print datetime.datetime.now().time()