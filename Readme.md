# README #

### What is this repository for? ###

Solutions to assignments of the course Artificial Intelligence from Bachelor Computer Science at University Antwerp.

The first four assignments are solutions to the questions from UC Berkeley's Intro to AI (http://inst.eecs.berkeley.edu/~cs188/pacman/search.html).

The fifth assignment is a solution to a competition from Kaggle about classifying fish (https://www.kaggle.com/c/the-nature-conservancy-fisheries-monitoring).

### How do I get set up? ###

All scripts can be run with Python3. 
You may additionally need OpenCV, Scikit-Learn and Numpy.
